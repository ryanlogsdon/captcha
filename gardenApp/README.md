Setting up the environment
--------------------------

node -v                         # we need at least node v8
npm -v                          # we need at least npm v6
npm install -g @angular/cli


Setting up the project
----------------------

    * ng new gardenApp                      # this will create a directory and the project within
        * add Angular routing?  Yes
        * which stylesheet?     CSS         # feel free to change this to SCSS!!

    * cd garden

    * run `ng serve --port 4202 --open` in another terminal tab :)



Remove boilerplate code
-----------------------
    * in src/app/app.component.html, only keep "<router-outlet></router-outlet>"



Adding components, services, and a Firebase auth & auth guard
-------------------------------------------------------------
    ng generate component home                              # main page

    * open `src > app > app-routing.module.ts`
        * import each component (just the 'home' component in this example)
        * add each component to the routes array (just 'home' in this example)
        

Add ReCAPTCHA
-------------

    * get a reCaptcha v2 key here https://www.google.com/recaptcha/admin/create   

    * npm i ng-recaptcha --save                   # https://www.npmjs.com/package/ng-recaptcha

    * app.module.ts > import the recaptura, then add to "imports"

    * import the callback to to whichever *.component.ts page needs it & the <re-captcha> tag to *.component.html
